# Build image.
build:
	docker build -t ternau/wwf_terr_ecoregions_rdf_dataset .

run:
	docker run --rm -it -v ${PWD}:/home/app ternau/wwf_terr_ecoregions_rdf_dataset

# Push to Docker Hub.
push:
	docker push ternau/wwf_terr_ecoregions_rdf_dataset:latest