# WWF Terrestrial Ecoregions of the World

This Python program reads in the shapefiles of the WWF Terr. Ecoregions of the World and converts it to an RDF dataset in GeoSPARQL. 