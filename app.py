from rdflib import Graph, Namespace, Literal
from rdflib.namespace import RDF, RDFS
from shapely.geometry import MultiPolygon, Polygon, shape

import logging
import time

from utils import get_shp

URL = 'https://swift.rc.nectar.org.au/v1/AUTH_05bca33fce34447ba7033b9305947f11/ultimate_feature_of_interest_shapefiles/wwf_terrestrial_ecoregions_2011/wwf_terr_ecos_oRn.shp'
BASE_URI = 'http://linked.data.gov.au/dataset/wwf-terr-ecoregions/'

GEOSPARQL = Namespace('http://www.opengis.net/ont/geosparql#')


def add_feature(g : Graph, feature, BASE : Namespace):
    uri = BASE[str(feature['properties']['OBJECTID'])]
    label = Literal(feature['properties']['ECO_NAME'])
    comment = Literal(feature['properties']['eco_code'])
    geometry_uri = BASE[str(feature['properties']['OBJECTID']) + '-geometry']
    g.add((uri, RDF.type, GEOSPARQL.Feature))
    g.add((uri, RDFS.label, label))
    g.add((uri, RDFS.comment, comment))
    g.add((uri, GEOSPARQL.hasGeometry, geometry_uri))

    if feature['geometry']['type'] == 'MultiPolygon':
        multipolygon = MultiPolygon(shape(feature['geometry']))
        g.add((geometry_uri, RDF.type, GEOSPARQL.Geometry))
        g.add((geometry_uri, RDFS.label, Literal(label)))
        g.add((geometry_uri, GEOSPARQL.asWKT, Literal(str(multipolygon.wkt), datatype=GEOSPARQL.wktLiteral)))
    elif feature['geometry']['type'] == 'Polygon':
        polygon = Polygon(shape(feature['geometry']))
        g.add((geometry_uri, RDF.type, GEOSPARQL.Geometry))
        g.add((geometry_uri, RDFS.label, Literal(label)))
        g.add((geometry_uri, GEOSPARQL.asWKT, Literal(str(polygon.wkt), datatype=GEOSPARQL.wktLiteral)))
    else:
        raise Exception(f'Geometry type not implemented. {feature["geometry"]["type"]}')


if __name__ == '__main__':
    start_time = time.time()
    logging.basicConfig(level=logging.INFO)

    BASE = Namespace(BASE_URI)
    shp = get_shp(URL)

    g = Graph()
    g.bind('geo', GEOSPARQL)

    for i, feature in enumerate(shp):
        logging.info(f'{i + 1} Processing feature {feature["properties"]}')

        add_feature(g, feature, BASE)

    logging.info('Serializing to disk as output.ttl')
    g.serialize('output.ttl', format='turtle')

    logging.info(f'Completed in {(time.time() - start_time):.2f} seconds')